import discord
from discord.ext import commands
import random

class SongIdeaGeneratorDebug(commands.Cog):
    
    def __init(self, client):    
        self.client = client

    @commands.Cog.listener()
    async def on_ready(self):
        print("Module: SongIdeaGeneratorDebug")

    @commands.command()
    async def list_genres(self, ctx):
        msg = ''
        with open('data/genres.txt', 'r') as file:
            genres = file.read().split('\n')[:-1]
            for index, genre in enumerate(genres):
                msg += f'{index}\t{genre}\n'
        await ctx.send(msg)

    @commands.command()
    async def list_songs(self, ctx):
        msg = ''
        with open('data/songs.txt', 'r') as file:
            songs = file.read().split('\n')[:-1]
            for song in songs:
                msg += f'\n{song}'
        await ctx.send(msg)

    @commands.command()
    async def list_restrictions(self, ctx):
        msg = ''
        with open('data/restrictions.txt', 'r') as file:
            restrictions = file.read().split('\n')[:-1]
            for restriction in restrictions:
                msg += f'\n{restriction}'
        await ctx.send(msg)

    @commands.command()
    async def list_acts(self, ctx):
        msg = ''
        with open('data/acts.txt', 'r') as file:
            acts = file.read().split('\n')[:-1]
            for act in acts:
                msg += f'\n{act}'
        await ctx.send(msg)

#    @commands.command()
#    async def rm_genre(self, ctx, indexes_to_delete):
#
#        num_of_lines = []
#        with open('data/genres.txt', 'r') as file:
#            num_of_lines = file.readlines()
#
#        with open('data/genres.txt', 'w') as file:
#
#            print(num_of_lines)
#            for index, line in enumerate(num_of_lines):
#                print(index, line)
#                    #file.write(line)
#
#            await ctx.send('Updated genres file')

def setup(client):
    client.add_cog(SongIdeaGeneratorDebug(client))
