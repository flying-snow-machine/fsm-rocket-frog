import discord
from discord.ext import commands
import random
from pydub import AudioSegment

import matchering as mg

#mg.log(print)


class AutoMastering(commands.Cog):
    
    def __init(self, client):    
        self.client = client

    @commands.Cog.listener()
    async def on_ready(self):
        print("Module: AutoMastering")

    @commands.command(brief="Set reference track")
    async def reference(self, ctx):
        file = ctx.message.attachments
        await file[0].save('audio/reference.mp3')
        ref_file = AudioSegment.from_file("audio/reference.mp3", format="mp3")
        ref_file.export("audio/reference.wav", format="wav")
        await ctx.send("Reference track set")

    @commands.command(brief="Show repo url")
    async def master(self, ctx):
        await ctx.send("Mastering track... this might take a while")
        file = ctx.message.attachments
        await file[0].save('audio/unmastered.mp3')

        unmastered_file= AudioSegment.from_file("audio/unmastered.mp3", format="mp3")
        unmastered_file.export("audio/unmastered.wav", format="wav")

        mg.process(
            # The track you want to master
            target="audio/unmastered.wav",
            # Some "wet" reference track
            reference="audio/reference.wav",
            # Where and how to save your results
            results=[
                mg.pcm16("audio/mastered.wav"),
                mg.pcm24("my_song_master_24bit.wav"),
            ],
        )

        mastered_file= AudioSegment.from_file("audio/mastered.wav", format="wav")
        mastered_file.export("audio/mastered.mp3", format="mp3")

        await ctx.send(file=discord.File('./audio/mastered.mp3'))

def setup(client):
    client.add_cog(AutoMastering(client))
