import discord
from discord.ext import commands
import random

class SongIdeaGenerator(commands.Cog):
    
    def __init(self, client):    
        self.client = client

    @commands.Cog.listener()
    async def on_ready(self):
        print("Module: SongIdeaGenerator")

    @commands.command(brief='Alias for songidea')
    async def frog(self, ctx):
        await self.songidea(ctx)

    @commands.command(brief='Generate a song idea')
    async def songidea(self, ctx):
        with open('data/genres.txt', 'r') as file:
            genres = file.read().split('\n')[:-1]

        with open('data/restrictions.txt', 'r') as file:
            restrictions = file.read().split('\n')[:-1]

        with open('data/acts.txt', 'r') as file:
            acts = file.read().split('\n')[:-1]

        genre1 = random.choice(genres)
        genre2 = random.choice(genres)
        genre3 = random.choice(genres)
        restriction = random.choice(restrictions)
        act = random.choice(acts)

        if random.randint(0,4) == 0:
            msg = act
        else:
            if random.randint(0,2) == 2:
                msg = f"Your idea for a new song will be a combination of **{genre1}**, **{genre2}**, and **{genre3}**." 
            else:
                msg = f"Your idea for a new song will be a combination of **{genre1}** and **{genre2}**." 

            #msg = f"Your idea for a new song will be a combination of **{genre1}** and **{genre2}**. Your restriction making this song is: **{restriction}**"
    
        await ctx.send(msg)

def setup(client):
    client.add_cog(SongIdeaGenerator(client))
