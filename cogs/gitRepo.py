import discord
from discord.ext import commands
import random

class GitRepo(commands.Cog):
    
    def __init(self, client):    
        self.client = client

    @commands.Cog.listener()
    async def on_ready(self):
        print("Module: GitRepo")

    @commands.command(brief="Show repo url")
    async def repo(self, ctx):
        msg = 'https://gitlab.com/flying-snow-machine/rocket-frog'
        await ctx.send(msg)

def setup(client):
    client.add_cog(GitRepo(client))
