import discord
from discord.ext import commands
import random
import json

class FeedbackLevels(commands.Cog):
    
    def __init(self, client):    
        self.client = client

    async def update_users(self, users, user):
        if not f'{user.id}' in users:
            users[f'{user.id}'] = {}
            users[f'{user.id}']['experience'] = 0

    async def add_experience(self, users, user, exp):
        users[f'{user.id}']['experience'] += exp

    @commands.Cog.listener()
    async def on_ready(self):
        print("Module: FeedbackLevels")


    @commands.command(brief='Alias for feedback')
    async def fb(self, ctx):
        await self.feedback(ctx)

    @commands.command(brief='Give feedback for levels')
    async def feedback(self, ctx):

        with open('data/users.json', 'r') as f:
            users = json.load(f)

        await self.update_users(users, ctx.author)
        await self.add_experience(users, ctx.author, len(ctx.message.content))

        with open('data/users.json', 'w') as f:
            json.dump(users, f)

        await ctx.send(users)


def setup(client):
    client.add_cog(FeedbackLevels(client))
