#!/usr/bin/env python

import os
import discord
from discord.ext import commands
from dotenv import load_dotenv
from pydub import AudioSegment

load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')

client = commands.Bot(command_prefix = '.')

@client.command(brief="Load clog module")
async def load(ctx, extension):
    client.load_extension(f'cogs.{extension}')

@client.command(brief="Unload clog module")
async def unload(ctx, extension):
    client.unload_extension(f'cogs.{extension}')

@client.command(brief="Reload all modules", aliases=['r'])
async def reload(ctx):
    for filename in os.listdir('./cogs'):
        if filename.endswith('.py'):
            client.unload_extension(f'cogs.{filename[:-3]}')
            client.load_extension(f'cogs.{filename[:-3]}')
    await ctx.send("Reloaded cogs")

@client.command(brief="List modules to load/unload")
async def list_cogs(ctx):
    msg = ''
    for filename in os.listdir('./cogs'):
        if filename.endswith('.py'):
            msg += f'\n{filename[:-3]}'
    #await ctx.send(msg)

for filename in os.listdir('./cogs'):
    if filename.endswith('.py'):
        client.load_extension(f'cogs.{filename[:-3]}')

@client.event
async def on_command_error(ctx, error):
    await ctx.send("I don't know that command...\nType .help for a list of commands.")
    

client.run(TOKEN)
